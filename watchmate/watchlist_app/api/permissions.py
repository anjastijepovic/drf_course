from rest_framework import permissions

# CUSTOM PERMISSION

# if user is admin, they can edit the review, if not, the review is read only

class IsAdminOrReadOnly(permissions.IsAdminUser):
    # moze da naslijedi i iz drugih klasa npr permissions.BasePermission ..

    def has_permission(self, request, view):
        admin_permission = bool(request.user and request.user.is_staff)
        # da li imamo loggedi in usera koji je takodje i admin
        return request.method == 'GET' or admin_permission
        # vraca true or false
        # ili ovako kako sam ovu donju klasu definisala
    
# ovu metodu importovati u views.py
    

class IsRevieWUserOrReadOnly(permissions.BasePermission):
    # samo review user moze editovati svoj review, drugi mogu samo da ga pogledaju

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            # Check permissions for read-only req, safe method is get, the others are unsafe
            return True
        else:
            # Check permissions for write request
            return obj.review_user == request.user
            # provjeriti je li user koji je napisao review isti kao current logged in user