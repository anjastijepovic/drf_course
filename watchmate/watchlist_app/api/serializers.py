# framework imports
from rest_framework import serializers

# app imports
from watchlist_app.models import WatchList, StreamPlatform, Review

# def name_length(value):
#         if len(value) < 2:
#             raise serializers.ValidationError("Name is too short")
#         return value

# class MovieSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     name = serializers.CharField(validator=[name_length])
#     description = serializers.CharField()
#     active = serializers.BooleanField()

#     def create(self, validated_data):
#         return Movie.objects.create(**validated_data)


#     def update(self, instance, validated_data):
#         # instance has old data, validated_data is new data
#         instance.name = validated_data.get('name', instance.name)
#         instance.description = validated_data.get('description', instance.description)
#         instance.active = validated_data.get('active', instance.active)
#         instance.save()
#         return instance
    
#     # VALIDATION

#     # Object-level validator
#     def validate(self, data):
#         if data['name'] == data['description']:
#             raise serializers.ValidationError("Name and description should be different")
#         return data
    
    # Field-level validator
    # def validate_name(self, value):

    #     if len(value) < 2:
    #         raise serializers.ValidationError("Name is too short")
    #     return value
    
    # it can be done with a validator arg as well
# promijenili model kasnije

# MODEL SERIALIZER

class ReviewSerializer(serializers.ModelSerializer):
    review_user = serializers.StringRelatedField(read_only=True)
    
    class Meta:
        model = Review
        # fields = "__all__"
        exclude =  ('watchlist',)

class WatchListSerializer(serializers.ModelSerializer):
    # custom serializer field
    # len_name = serializers.SerializerMethodField()
    # reviews = ReviewSerializer(many=True, read_only=True)
    # commented the reviews so we dont see them when we send a get request to watch/list2/
    # jedan film moze imati vise reviews
    # read-only - when i send a post req while adding a movie, series, podcast (watchlists), we are not gonna add a review, just the other watchlist fields
    # we cannot add the rew through the serializer

    platform = serializers.CharField(source='platform.name')
    # overwriting the platform field so it is a name instead of an id

    class Meta:
        model = WatchList
        fields = "__all__" #a moze i lista fields ili exclude
        
    # def get_len_name(self, object):
    #     return len(object.name)
    
    # # Object-level validator
    # def validate(self, data):
    #     if data['name'] == data['description']:
    #         raise serializers.ValidationError("Name and description should be different")
    #     return data
    
    # # Field-level validator
    # def validate_name(self, value):

    #     if len(value) < 2:
    #         raise serializers.ValidationError("Name is too short")
    #     return value
    
class StreamPlatformSerializer(serializers.ModelSerializer):
    watchlist = WatchListSerializer(many=True, read_only=True)
    # nested veza u serializers, da bi platforma imala field sa watchlist listom
    # mora ime watchlist jer je to related_name iz modela

    class Meta:
        model = StreamPlatform
        fields = "__all__" 