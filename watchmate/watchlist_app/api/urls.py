from django.urls import path, include
from rest_framework.routers import DefaultRouter
from watchlist_app.api import views

router = DefaultRouter()
router.register('stream', views.StreamPlatformVS, basename='streamplatform')

urlpatterns = [
    path('', views.WatchListAV.as_view(), name='movie-list'),  #get all movies or post a new one, http://127.0.0.1:8000/watch
    path('<int:pk>/', views.WatchDetailAV.as_view(), name='movie-detail' ), # get movie details

    # path('stream/', StreamPlatformAV.as_view(), name='stream' ),
    # path('stream/<int:pk>', StreamPlatformDetailAV.as_view(), name='stream-detail' ), #id streama
    # these two can be combined w router:
    path('', include(router.urls)), #get and post stream platforms

    path('<int:pk>/reviews/create/', views.ReviewCreate.as_view(), name='review-create' ), #create reviews for a particular movie
    path('<int:pk>/reviews/', views.ReviewList.as_view(), name='review-list' ), #access reviews for a particular watchlist with the pk
    path('reviews/<int:pk>/', views.ReviewDetail.as_view(), name='review-detail' ), #access individual review
    #path('reviews/<str:username>/', UserReview.as_view(), name='user-review-detail' ), #access all reviews for a user, filtering against user
    path('user-reviews/', views.UserReview.as_view(), name='user-review-detail' ), #access all reviews for a user, filtering against query param, ?username=anjas,
    path('list2/', views.WatchListGV.as_view(), name='watch-list' ), #get all movies or post a new one, example for filtering

]
