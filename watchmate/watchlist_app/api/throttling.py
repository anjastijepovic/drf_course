from rest_framework.throttling import UserRateThrottle
# da user moze da ostavi 2 reviews per day, i 10 puta da vidi listu reviews. def u settings.py

class ReviewCreateThrottle(UserRateThrottle):
    scope = 'review-create'

class ReviewListThrottle(UserRateThrottle):
    scope = 'review-list'