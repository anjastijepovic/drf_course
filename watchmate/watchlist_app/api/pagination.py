from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination, CursorPagination


class WatchListPagination(PageNumberPagination):
    # you can owerwrite diff things from PageNUmberPagination
    page_size = 5
    page_size_query_param = 'size'
    # customization of size for the client
    #  npr moze da posalje zahtjev watch/list2/?size=10
    max_page_size = 10
    # maksimalno moze deset elemenata na stranici


class WatchListLOPagination(LimitOffsetPagination):
    default_limit = 5
    max_limit = 10

class WatchListCPagination(CursorPagination):
    page_size = 5
    ordering = 'created' # da bude old to new
    cursor_query_param = 'record'
    # da users ne bi mogli da skip to the last page odma nego mogu samo next next