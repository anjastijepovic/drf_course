from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import ValidationError
# from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework import generics, mixins, viewsets
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.throttling import UserRateThrottle, AnonRateThrottle, ScopedRateThrottle
from rest_framework import filters
# za SerachFilter

from watchlist_app.api  import pagination, permissions, serializers, throttling
from watchlist_app.models import WatchList, StreamPlatform, Review


# FUNCTION BASED VIEWS

# @api_view(['GET', 'POST'])
# def movie_list(request):

#     if request.method == 'GET':
#         movies = Movie.objects.all()
#         serializer = serializers.MovieSerializer(movies, many=True)
#         return Response(serializer.data)

#     if request.method == 'POST':
#         serializer = serializers.MovieSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors)


# @api_view(['GET', 'PUT', 'DELETE'])
# def movie_details(request, pk):

#     if request.method == 'GET':

#         try:
#             movie = Movie.objects.get(pk=pk)
#         except Movie.DoesNotExist:
#             return Response({'error': 'Movie not found'}, status=status.HTTP_404_NOT_FOUND)

#         serializer = serializers.MovieSerializer(movie)
#         return Response(serializer.data)

#     if request.method == 'PUT':
#         movie = Movie.objects.get(pk=pk)
#         serializer = serializers.MovieSerializer(movie, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors)

#     if request.method == 'DELETE':
#         movie = Movie.objects.get(pk=pk)
#         movie.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)


    # CLASS BASED VIEWS


    # MIXINS

# class ReviewList(mixins.ListModelMixin,
#                  mixins.CreateModelMixin,
#                  generics.GenericAPIView):
#     queryset = Review.objects.all()
#     serializer_class = serializers.ReviewSerializer


#     def get(self, request, *args, **kwargs):
#         return self.list(request, *args, **kwargs)


#     def post(self, request, *args, **kwargs):
#         return self.create(request, *args, **kwargs)


# class ReviewDetail(mixins.RetrieveModelMixin,
#                  generics.GenericAPIView):
#     queryset = Review.objects.all()
#     serializer_class = serializers.ReviewSerializer


#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)


    # GENERIC API VIEWS

class ReviewList(generics.ListAPIView):
    # queryset = Review.objects.all()
    # treba da overwritujemo queryset jer ne zelimo da nam
    serializer_class = serializers.ReviewSerializer
    throttle_classes = [throttling.ReviewListThrottle, AnonRateThrottle]
    # # DjangoFilterBackend:
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['review_user__username', 'active']
    # filter po useru ili po active. u Postmanu npr: watch/5/reviews/?active=true&review_user__username=anjas

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Review.objects.filter(watchlist=pk)


class ReviewCreate(generics.CreateAPIView):
    # odvojili od prethodne jer bismo inace morali da napisemo u json body watchlist i njegov id, a mi zelimo da direktno ako podjemo na stream/pk/review-create da dodamo film or wtv
    serializer_class = serializers.ReviewSerializer
    permission_classes = [IsAuthenticated]
    throttle_classes = [throttling.ReviewCreateThrottle]

    def get_queryset(self):
        return Review.objects.all()

    # we need to overwrite the create method cause we need to pass the specific movie id

    def perform_create(self, serializer):
        pk = self.kwargs['pk']
        movie = WatchList.objects.get(pk=pk)

        # ako je user vec ostavio review, ne moze opet da ga ostavi
        review_user = self.request.user
        review_queryset = Review.objects.filter(
            watchlist=movie, review_user=review_user)

        if review_queryset.exists():
            raise ValidationError('You have already reviewed this movie')

        #  increase the number_rating by one i update avg
        if movie.number_rating == 0:
            # ako je broj reviews 0,
            # avg rating je tada isto nula, pa onda direktno treba da pass novi rating, ne racunamo avg
            movie.avg_rating = serializer.validated_data['rating']
        else:
            # ako nije, moramo da calculate the average, stari + novi rating / 2
            movie.avg_rating = (movie.avg_rating +
                                serializer.validated_data['rating'])/2
        movie.number_rating = movie.number_rating + 1
        movie.save()

        serializer.save(watchlist=movie, review_user=review_user)


class ReviewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Review.objects.all()
    serializer_class = serializers.ReviewSerializer
    permission_classes = [permissions.IsRevieWUserOrReadOnly]
    throttle_classes = [ScopedRateThrottle]
    throttle_scope = 'review-detail'
    # view 2 review detail per day u settings


class WatchListGV(generics.ListAPIView):
    queryset = WatchList.objects.all()
    serializer_class = serializers.WatchListSerializer

    # # Search: {{baseurl}}watch/list2/?search=python
    # filter_backends = [filters.SearchFilter]
    # search_fields = ['title', '=platform__name']

    # # Ordering: {{baseurl}}watch/list2/?ordering=avg_rating
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['-avg_rating']
    pagination_class = pagination.WatchListPagination


    # APIView

class WatchListAV(APIView):
    permission_classes = [permissions.IsAdminOrReadOnly]

    def get(self, request):
        movies = WatchList.objects.all()
        serializer = serializers.WatchListSerializer(movies, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = serializers.WatchListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class WatchDetailAV(APIView):
    permission_classes = [permissions.IsAdminOrReadOnly]

    def get(self, request, pk):
        try:
            movie = WatchList.objects.get(pk=pk)
        except WatchList.DoesNotExist:
            return Response({'error': 'Not found'}, status=status.HTTP_404_NOT_FOUND)

        serializer = serializers.WatchListSerializer(movie)
        return Response(serializer.data)

    def put(self, request, pk):
        movie = WatchList.objects.get(pk=pk)
        serializer = serializers.WatchListSerializer(movie, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

    def delete(self, request, pk):
        movie = WatchList.objects.get(pk=pk)
        movie.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# class StreamPlatformAV(APIView):
#     permission_classes = [IsAdminOrReadOnly]

#     def get(self, request):
#         platform = StreamPlatform.objects.all()
#         serializer = serializers.StreamPlatformSerializer(platform, many=True)
#         return Response(serializer.data)

#     def post(self, request):
#         serializer = serializers.StreamPlatformSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors)


# class StreamPlatformDetailAV(APIView):
#     permission_classes = [IsAdminOrReadOnly]

#     def get(self, request, pk):
#         try:throttling.xist:
#             return Response({'error': 'Not found'}, status=status.HTTP_404_NOT_FOUND)

#         serializer = serializers.StreamPlatformSerializer(platform)
#         return Response(serializer.data)

#     def put(self, request, pk):
#         platform = StreamPlatform.objects.get(pk=pk)
#         serializer = serializers.StreamPlatformSerializer(platform, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors)

#     def delete(self, request, pk):
#         platform = StreamPlatform.objects.get(pk=pk)
#         platform.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)


    # VIEWSETS

# class StreamPlatformVS(viewsets.ViewSet):


#     def list(self, request):
#         queryset = StreamPlatform.objects.all()
#         serializer = StreamPlatformSerializer(queryset, many=True)
#         return Response(serializer.data)


#     def retrieve(self, request, pk=None):
#         queryset = StreamPlatform.objects.all()
#         watchlist = get_object_or_404(queryset, pk=pk)
#         serializer = StreamPlatformSerializer(watchlist)
#         return Response(serializer.data)


    # MODELVIEWSETS

# ne definisemo sve metode posebno kao u ViewSet
class StreamPlatformVS(viewsets.ModelViewSet):
    queryset = StreamPlatform.objects.all()
    serializer_class = serializers.StreamPlatformSerializer
    permission_classes = [permissions.IsAdminOrReadOnly]


    # FILTERING

class UserReview(generics.ListAPIView):
    serializer_class = serializers.ReviewSerializer

    # filtering against user, path watch/reviews/anjas/

    # def get_queryset(self):
    #     username = self.kwargs['username']
    #     return Review.objects.filter(review_user__username=username)
    # jump onto review_user koji je foreign key, then jump onto username to match it ?
    # ugl moramo ovo __username jer je rev user foreign key u modelu Review, da trazimo neki drugi field, ne bi trebalo

    # filtering against query param, path watch/reviews/?username=anjas

    def get_queryset(self):
        username = self.request.query_params.get('username', None)
        return Review.objects.filter(review_user__username=username)
